<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckRevisorMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if( Auth::check() && Auth::user()->hasRole('revisor')  ) //negandolo il primo blocco sarà il falso
        {
            return $next($request);
        }

        return redirect()->route('home')->with('error','Non sei autorizzato ad entrare qua');

    }
}
