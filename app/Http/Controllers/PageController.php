<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function contacts()
    {
        return view('User.contacts.email');
    }

    public function locale($locale)
    {
        session()->put('locale',$locale);
        return redirect()->back();
    }
}
