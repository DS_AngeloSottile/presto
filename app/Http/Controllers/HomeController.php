<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Announcement;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $announcements = Announcement::SelectAnnouncements(1)->take(5)->get();

        return view('home',compact('announcements'));
    }


    public function searchByCategory($id)
    {
        $announcements = Announcement::where('categorie_id',$id)->SelectAnnouncements(1)->paginate(5);

        return view('home',compact('announcements'));
    }

    public function search(Request $request)
    {

        $whatToSearch = $request->q;

        $announcements = Announcement::search($whatToSearch)->orderBy('created_at','desc')->where('is_accepted',1)->get();

        return view('home',compact('announcements'));
    }
}
