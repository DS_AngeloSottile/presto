<?php

namespace App\Http\Controllers\Revisor;

use App\Http\Controllers\Controller;
use App\Models\Announcement;
use Illuminate\Http\Request;

class RevisorAnnouncementController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.revisor');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $announcementToRevisioned = Announcement::SelectAnnouncements(null)->first();

        $announcementsTrashed = Announcement::SelectAnnouncements(0)->paginate(2);

        return view('revisor.home',compact('announcementToRevisioned','announcementsTrashed'));
    }

    public  function setAnnouncementAcceptReject(Announcement $announcement,$value)
    {
        if(!$announcement)
        {
            $errors =
            [
                'error_message' => (__('ui.notFound'))
            ];

            session()->flash('error',$errors);
            return redirect()->route('revisor.index');
        }


        $announcement->update(
            [
                'is_accepted' => $value,
            ]
        );

        return redirect()->route('revisor.index');
    }


    public  function acceptAnnouncement(Announcement $announcement)
    {
        if(!$announcement)
        {
            $errors =
            [
                'error_message' => (__('ui.notFound'))
            ];

            session()->flash('error',$errors);
            return redirect()->route('revisor.index');
        }

        return $this->setAnnouncementAcceptReject($announcement,true); //accettato
    }


    public function rejecttAnnouncement(Announcement $announcement)
    {
        if(!$announcement)
        {
            $errors =
            [
                'error_message' => (__('ui.notFound'))
            ];

            session()->flash('error',$errors);
            return redirect()->route('revisor.index');
        }

        return $this->setAnnouncementAcceptReject($announcement,false); // scartato
    }


    public function restoreAnnouncement(Announcement $announcement)
    {
        if(!$announcement)
        {
            $errors =
            [
                'error_message' => (__('ui.notFound'))
            ];

            session()->flash('error',$errors);
            return redirect()->route('revisor.index');
        }

        return $this->setAnnouncementAcceptReject($announcement,null); // da revisionare
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteAnnouncement(Announcement $announcement)
    {
        if(!$announcement)
        {
            $errors =
            [
                'error_message' => (__('ui.notFound'))
            ];

            session()->flash('error',$errors);
            return redirect()->route('revisor.index');
        }

        $deleted = $announcement->delete();

        if($deleted )
        {
            session()->flash('success',( __('ui.destroy') ));
            return redirect()->route('revisor.index');
        }
        else
        {
            session()->flash('error',(__('ui.destroyError')));
            return redirect()->route('revisor.index');
        }
    }
}
