<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\EmailUserCreateRequest;
use App\Mail\UserRequestRevisor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
    public function sendEmail(EmailUserCreateRequest $request)
    {
        $user = Auth::user();

        $emailBody = $request->email;
        Mail::to('angelosottiled31@hotmail.it')->send(new UserRequestRevisor($emailBody,$user->email));

        return redirect()->route('home')->with("success", (__('ui.successEmail')));
    }
}
