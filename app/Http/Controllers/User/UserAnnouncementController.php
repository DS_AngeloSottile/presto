<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateAnnouncementRequest;
use App\Jobs\AddWatermark;
use App\Jobs\GoogleVisionLabelImage;
use App\Jobs\GoogleVisionSafeSearchImage;
use App\Jobs\GoogleVisioRemoveFaces;
use App\Jobs\ResizeImage;
use App\Models\Announcement;
use App\Models\AnnouncementImage;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Spatie\Image\Image;
use Spatie\Image\Manipulations;

class UserAnnouncementController extends Controller
{
    public function __construct()
    {
        $this->middleware('verified')->except(['show','searchByCategory']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $categories = Category::all();


        $uniqueSecret = $request->old('uniqueSecret', base_convert( sha1( uniqid( mt_rand() ) ), 16,36 ) );

        return view('User.Announcement.create',compact('categories','uniqueSecret'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateAnnouncementRequest $request)
    {
        try
        {
            DB::beginTransaction();

                $user = Auth::user();

                $announcement = $user->announcements()->create(
                    [
                        'title'  => $request->title,
                        'body'  => $request->body,
                        'price' => $request->price,
                        'slug'  => uniqid(),
                    ]
                ); //se questa viene eseguita   ->    annullata
                //CRASH
                $announcement->category()->associate($request->categories); //questa torna indietro
                $announcement->save();

                $uniqueSecret = $request->uniqueSecret;

                $images = session()->get("images.{$uniqueSecret}",[]); //se non sono stati settati verrà preso un array vuoto
                $removedImages = session()->get("removedImages.{$uniqueSecret}",[]); //se non sono stati settati verrà preso un array vuoto

                $images = array_diff($images,$removedImages); //prendiamo solo le immagini presenti in temp

                if($images)
                {
                    foreach($images as $image)
                    {
                        $amnouncementImage = new AnnouncementImage(); //ci creiamo l'immagine

                        $fileName = basename($image); //dentro image c'è il path, con basename prendiamo solo il nome
                        $newFileName = "public/announcements/{$announcement->id}/{$fileName}"; //il nuovo path dove deve andare l'immagine
                        $moved = Storage::move($image,$newFileName); //lo muoviamo dalla cartella temp alla nuova cartelal con l'id dell'annuncio

                        if($moved)
                        {   //questi vengono fatti uno dopo l'altro
                            $amnouncementImage->file = $newFileName;
                            $amnouncementImage->announcement_id = $announcement->id;
                            $amnouncementImage->save();
                            //questi vengono fatti uno dopo l'altro

                            GoogleVisionSafeSearchImage::withChain( //mentre quelle sopra stanno facendo, si atttivano piano piano anche queste
                                [
                                    new GoogleVisionLabelImage( $amnouncementImage->id ),
                                    new GoogleVisioRemoveFaces( $amnouncementImage->id ),
                                    new ResizeImage($amnouncementImage->file,300,150),
                                ]
                            )->dispatch( $amnouncementImage->id);
                            //se io scrivo qua del codice, verrà processato senza la sicurezza che i jobs dietro siano terminati
                        }
                        else
                        {
                            return redirect()->route('home')->with('error',(__('ui.announcementPubblishError')));
                        }
                    }

                    File::deleteDirectory(storage_path("/app/public/temp/{$uniqueSecret}"));
                }

            DB::commit();

                return redirect()->route('home')->with('success',(__('ui.announcementPubblish')));

        }
        catch(\Exception $e)
        {

            $errors =
            [
                'error_message' => (__('ui.announcementPubblishError')),
            ];

            session()->flash('error',$errors);
            return redirect()->route('home');

        }

    }

    public function deleteImage(Request $request)
    {
        $uniqueSecret = $request->uniqueSecret; // il form LE IMMAGINI SONO CONSERVATE IN UNA CARTELLE AVENTE IL NOME DEL FORM

        $fileName = $request->id; //PATH
        //poichè l'immagine prima di essere stata cancellata è stata inserita nello storage, si dovrà creare un un nuovo array contenente
        //le immagine scartate in modo da eliminare quelle duplicate
        session()->push("removedImages.{$uniqueSecret}",$fileName);

        Storage::delete($fileName);

        return response()->json('ok');
    }


    public function getImages(Request $request)
    {
        //dd($request->all());

        $uniqueSecret = $request->uniqueSecret;

        $images = session()->get("images.{$uniqueSecret}",[]);
        $removedImages = session()->get("removedImages.{$uniqueSecret}",[]);

        $images = array_diff($images,$removedImages);

        $data = [];

        foreach($images as $image)
        {
            $data[] =
            [
                'id'    => $image,
                'src'   => AnnouncementImage::getUrlByFilePath($image,80,80)
            ];
        }

        return response()->json($data);
    }

    public function uploadImage(Request $request)
    {
        $uniqueSecret = $request->uniqueSecret; //questo è passato dalla richiesta in announcementImages.js

        $fileName = $request->file('file')->store("public/temp/{$uniqueSecret}"); //ogni volta che si clicca su un nuovo file verrà inserito nella cartella con il nome del uniqueSecret

        dispatch(new ResizeImage(
            $fileName,
            80,
            80
        ));

        session()->push("images.{$uniqueSecret}",$fileName); // oltre a mettere i file nello store, li mettiamo in sessione

        return response()->json(
            [
                'id'    =>  $fileName, //ritorniamo il path del file sotto nome 'id', questo serve SOLO per identificare quell'immagine in modo da cancellarla, o riprenderla
            ]
        );
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Announcement $announcement)
    {
        return view('User.announcement.show',compact('announcement'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
