<?php

namespace App\Providers;

use App\Models\Announcement;
use App\Models\Category;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrap();


        if ( Schema::hasTable('categories') && Schema::hasTable('announcements') )
        {
            $categories = Category::all();
            $announcementsAccepted = Announcement::announcementsAcceptedCount();

            view()->share('categories', $categories);
            view()->share('announcementsAccepted', $announcementsAccepted);
        }
    }
}
