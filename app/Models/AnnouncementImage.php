<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class AnnouncementImage extends Model
{
    use HasFactory;

    protected $casts = //mutators    i mutators prendono il nome della colonna. La colonna diventa.... il secondo attributo che hai scritto
                       //nel nostro caso un array
    [
        'labels' =>'array',
    ];


    protected $fillable =
    [
        'announcement_id',
        'file',
    ];


    public function getAdultAttribute( $value)
    {
        $this->myAttributesColumns($value);
    }


    public function getSpoofAttribute( $value)
    {
        $this->myAttributesColumns($value);
    }


    public function getMedicalAttribute( $value)
    {
        $this->myAttributesColumns($value);
    }


    public function getViolenceAttribute( $value)
    {
        $this->myAttributesColumns($value);
    }


    public function getRacyAttribute( $value)
    {
        $this->myAttributesColumns($value);
    }


    public function myAttributesColumns($value)
    {
        switch ($value) {
            case "UNKNOWN":
                echo "
                <div class='progress'>
                    <div class='progress-bar bg-info' role='progressbar' style='width: 5%' aria-valuenow='50' aria-valuemin='0' aria-valuemax='100'></div>
                </div>
                ";
                break;
            case "VERY_UNLIKELY":
                echo "
                <div class='progress'>
                    <div class='progress-bar bg-success' role='progressbar' style='width: 5%' aria-valuenow='50' aria-valuemin='0' aria-valuemax='100'></div>
                </div>
                ";
                break;
            case ("UNLIKELY" || "POSSIBLE"):
                echo "
                <div class='progress'>
                    <div class='progress-bar bg-warning' role='progressbar' style='width: 66%' aria-valuenow='50' aria-valuemin='0' aria-valuemax='100'></div>
                </div>";
                break;
            case ("LIKELY" || "VERY_LIKELY"):
                echo "
                <div class='progress'>
                    <div class='progress-bar bg-danger' role='progressbar' style='width: 100%' aria-valuenow='50' aria-valuemin='0' aria-valuemax='100'></div>
                </div>";
                break;
        }
    }


    static public function getUrlByFilePath($filepath,$w = null,$h = null)
    {

        if(!$w && !$h)
        {
            return Storage::url($filepath);
        }

        $path = dirname($filepath);
        $filename = basename($filepath);

        $file = "{$path}/crop{$w}x{$h}_{$filename}";

        return Storage::url($file);
    }

    public function getUrl($w = null,$h = null)
    {
        return AnnouncementImage::getUrlByFilePath($this->file,$w,$h);
    }


    public function announcement()
    {
        return $this->belongsTo('App\Models\Announcement');
    }
}
