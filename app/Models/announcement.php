<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Laravel\Scout\Searchable;

class Announcement extends Model
{
    use HasFactory,Searchable;

    protected $fillable =
    [
        'title',
        'body',
        'is_accepted',
        'price',
        'user_id',
        'categorie_id',
        'slug',
    ];

     protected $with =
    [
        'category',
        'user',
        'images',
    ];

    public function toSearchableArray()
    {
        $category = $this->category;
        $user = $this->user;

        $array = [
            'id'            => $this->id,
            'title'         => $this->title,
            'body'          => $this->body,
            'category'      => $category,
            'user'      => $user,
        ];

        return $array;
    }

    public function setTitleAttribute($value) //nome colonna grande tra set e attribute, il contenuto verrà catturato e verrà eseguita l'extra logica
    {
        return $this->attributes['title'] = strtoupper($value);
    }

    public function setPriceAttribute($value) //nome colonna grande tra set e attribute, il contenuto verrà catturato e verrà eseguita l'extra logica
    {
        return $this->attributes['price'] = $value.'€';
    }

    public static function announcementsAcceptedCount()
    {
        return Announcement::where('is_accepted',null)->count();
    }


    public function scopeSelectAnnouncements($query,$status)
    {
        return $query = $query->orderBy('created_at','desc')->where('is_accepted',$status);
    }



    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class,'categorie_id','id');
    }

    public function images()
    {
        return $this->hasMany('App\Models\AnnouncementImage');
    }



    public function getRouteKeyName()
    {
        return 'slug';
    }

}
