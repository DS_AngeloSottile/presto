<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserRequestRevisor extends Mailable
{
    use Queueable, SerializesModels;

    private $body;
    private $userEmail;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($body,$userEmail)
    {
        $this->body = $body;
        $this->userEmail = $userEmail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('User.emails.requestRevisor',
            [
                'body'          => $this->body,
                'userEmail'     => $this->userEmail,
            ]
        );
    }
}
