<?php

namespace App\Jobs;

use App\Models\AnnouncementImage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Google\Cloud\Core\ServiceBuilder;
use Google\Cloud\Vision\V1\ImageAnnotatorClient;

class GoogleVisionSafeSearchImage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $announcement_image_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($announcement_image_id)
    {
        $this->announcement_image_id = $announcement_image_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $announcementImage = AnnouncementImage::find($this->announcement_image_id);

        if(!$announcementImage)
        {
            return;
        }

        $image = file_get_contents(storage_path('app/'.$announcementImage->file));
        putenv('GOOGLE_APPLICATION_CREDENTIALS='.base_path('google_credentials.json'));

        $imageAnnotator = new ImageAnnotatorClient();
        $response = $imageAnnotator->safeSearchDetection($image);
        $imageAnnotator->close();

        $safe = $response->getSafeSearchAnnotation();

        $adult = $safe->getAdult();
        $medical = $safe->getMedical();
        $spoof = $safe->getSpoof();
        $violence = $safe->getViolence();
        $racy = $safe->getRacy();

        //echo json_encode([$adult,$medical,$spoof,$violence,$racy]);

        $likelihoodName =[
            //bianco    //verde          //giallo                  rosso
            'UNKNOWN','VERY_UNLIKELY','UNLIKELY','POSSIBLE','LIKELY','VERY_LIKELY'
        ];


        $announcementImage->adult =     $likelihoodName[$adult];
        $announcementImage->medical =   $likelihoodName[$medical];
        $announcementImage->spoof =     $likelihoodName[$spoof];
        $announcementImage->violence =  $likelihoodName[$violence];
        $announcementImage->racy =      $likelihoodName[$racy];

        $announcementImage->save();

    }
}
