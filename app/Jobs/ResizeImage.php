<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Spatie\Image\Image;
use Spatie\Image\Manipulations;

class ResizeImage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $path, $fileName, $w, $h;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($filepath,$w,$h)
    {
        $this->path = dirname($filepath);//dir
        $this->fileName = basename($filepath); //nome
        $this->w = $w;
        $this->h = $h;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $w = $this->w;
        $h = $this->h;

        $srcPath = storage_path(). '/app'. '/' . $this->path. '/'. $this->fileName;

        $destPath = storage_path(). '/app'. '/' . $this->path. "/crop{$w}x{$h}_". $this->fileName;

        Image::load($srcPath)->crop(Manipulations::CROP_CENTER, $w,$h)->save($destPath);
        if($w == 300)
        {
            Image::load($destPath)->watermark(base_path('resources/img/watermark/watermark.png'))->watermarkOpacity(50)->watermarkPosition(Manipulations::POSITION_CENTER)->save($destPath);
        }


    }
}
