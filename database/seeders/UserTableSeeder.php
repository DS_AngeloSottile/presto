<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createAdmin();

        for($i=0;$i<5;$i++)
        {

            $user_data =
            [
                'role_id' => 2, //user
                'name' => 'Angelo'. $i,
                'email' =>'angelosottile'. $i.'@hotmail.it',
                'email_verified_at' =>Carbon::now(),
                'password' => Hash::make('password'),
                'remember_token' => Str::random(10),
            ];

            User::Create($user_data);

        }
    }

    private function createAdmin()
    {

        $admin_role = Role::where('code','revisor')->first();
        $user_data =
        [
            'name' => 'DS',
            'email' => 'ds@hotmail.it',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('password'), //così si generano le password di Laravel
            'remember_token' => Str::random(10),
        ];

        $user= User::Create($user_data);

        $user->role()->associate($admin_role);
        $user->save();

    }
}
