<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CreateCategoriesTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories =
        [
            ['name' => 'Giochi'],
            ['name' => 'Mobili'],
            ['name' => 'Video giochi'],
            ['name' => 'musica'],
            ['name' => 'automobili'],
            ['name' => 'elettronica'],
            ['name' => 'attrezzi'],
            ['name' => 'vestiti'],
            ['name' => 'artefatti antichi'],
            ['name' => 'sport'],
        ];
        Category::insert($categories);
    }
}
