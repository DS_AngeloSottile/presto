<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGooglevisionFieldsToAnnouncementImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('announcement_images', function (Blueprint $table) {
            $table->text("labels")->nullable();

            $table->text("adult")->nullable();
            $table->text("spoof")->nullable();
            $table->text("medical")->nullable();
            $table->text("violence")->nullable();
            $table->text("racy")->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('announcement_images', function (Blueprint $table) {
            $table->dropColumn(['labels','dult','spoof','medical','violence','racy']);
        });
    }
}
