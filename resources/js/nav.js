window.onscroll = function() 
{
    let navbar = document.querySelector(".my-navbar")

    if (document.documentElement.scrollTop > 50) 
    {
        navbar.classList.add("nav-background")
    } else 
    {
        navbar.classList.remove("nav-background")
    }

}