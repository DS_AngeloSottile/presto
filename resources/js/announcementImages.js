$(function()
{   //dopo che lap agina è stata caricata
    if( $('#drophere').length > 0 ) //sistema di jquey per prendere elementi del doom
    {
        let csrfToken = $('meta[name="csrf-token"]').attr('content');
        let uniqueSecret = $('input[name="uniqueSecret"]').attr('value');


        let myDropzone = new Dropzone('#drophere',
        {
            url:'/announcement/images/upload', //ci creiamo la rotta di laravel

            params: {
                _token: csrfToken, //solo per le chiamate in post
                //1° COSA CHE FA ANDARE IN CONFUSIONE
                uniqueSecret: uniqueSecret  //ad ogni uniqueSecret corrisponde un form. Grazie a questo è possibbile risalire a tutte le immagini di quel form
            },

            addRemoveLinks: true, //parte opzionale di dropzone per eliminare le immagini

            init: function()
            {
                $.ajax(
                {
                    type: 'GET',
                    url: '/announcement/images',
                    data:
                    {
                        uniqueSecret: uniqueSecret,
                    },
                    dataType: 'json',

                }).done( function(data)
                {
                    $.each( data, function(key,value)
                    {
                        let file =
                        {
                            serverId: value.id
                        };

                        myDropzone.options.addedfile.call(myDropzone,file);
                        myDropzone.options.thumbnail.call(myDropzone,file,value.src);
                    });
                });
            }

        });

        //queste 2 parti servono per eliminare l'immagine   2° COSA CHE FA ANDARE IN CONFUSIONE
        myDropzone.on('success',function(file,response) //questa funzione scatta se la chiamata in post è andata a buon fine
        {
            file.serverId = response.id // l'id passato da       questa rotta post /announcement/images/upload
        });
        //cosa fai con quell'id ?
        myDropzone.on("removedfile",function(file)
        {
            $.ajax(
            {
                type: 'DELETE',
                url: '/announcement/images/remove', //si richiama una rotta che elimina il file
                data:
                {
                    _token: csrfToken, // token, perchè la rotta di laravel lo richiede a prescindere
                    id: file.serverId, //identificativo di quell'immagine, ovvero solo il path
                    uniqueSecret: uniqueSecret, //codice del form
                },

                dataType: 'json',
            });
        });
    }
});
