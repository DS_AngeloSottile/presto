let imagesCarousel = document.querySelectorAll('#imgCarousel'); //immagini carousel
let spaceFreeForImage = document.querySelector('#secondImage'); //spazio dove inserire l'immagine

console.log(spaceFreeForImage);

init();

function init()
{

    imagesCarousel.forEach( (image) => //ad ogni immagine inserisco un evento di click
    {
        image.addEventListener('click',function()
        {
            addToSpace(image); //si occupa di inserire l'imamgine nell ospazio
        })
    });
}

function addToSpace(image)
{
    spaceFreeForImage.innerHTML = " "; //svuoto il contenuto

    let clone = image.cloneNode(true); //clono ol nodo

    clone.setAttribute('id','newImageAdded');
    clone.classList.remove('w-100');    //rimuovo la classe

    spaceFreeForImage.appendChild(clone); //lo appendo

}



