<?php

return [
    'welcome'   =>'Welcome to Presto.it!',
    'createAnnouncement' => 'Create announcement',
    'login' => 'Login',
    'register' => 'Register',
    'management' => 'Management',
    'admin' => 'Admin',
    'Logout' => 'Logout',
    'contact' => 'Contatact us',
    'search'=>'Search',
    // 'selectedcategory'=>'Selected category',
    'noAnnouncement'=>'There are no announcements to review',
    'discardAnnouncement'=>' There are non discarded announcements ',
    'details' => 'Details',
    'restore' => 'Restore',
    'delete'=>'Delete',
    'accept'=>'Accept',
    'reject'=>'Reject',
    'addToCart'=>'Add to cart',

    //create Announcement
    'createAnnouncement'        => 'Create Announcement',
    'allAnnouncements'          => 'All Announcements',

    'title'                     => 'title',
    'price'                     => 'price',
    'body'                      => 'body',
    'images'                    => 'images',
    'categories'                => 'categories',
    'submit'                    => 'submit',

    //Contacts
    'contacts'          =>'Do you want to become a manager? Contact us',
    'emailContent'      =>'Write us',


    //login
    'emailAddress'      =>  'email address',
    'forgotPassword'    =>  'Have you forgotten your password?',
    'rememberMe'        =>  'remember me',


    //register
    'name'                 =>'name',
    'confirmPassword'      =>'confirm password',


    //verivy
    'emailVerify'       => 'Verify Your Email Address',
    'emailBody'         => 'Before proceeding, please check your email for a verification link.If you did not receive the email,',
    'emailRequest'      => 'Click here to request another',


    //flash messages
    'checkVerify'                       => 'You need to verify your email link before access',
    'destroy'                           => 'Announcement destroyed',
    'destroyError'                      => 'Error on destroy announcement',
    'notFound'                          => 'Announcement not found',
    'successEmail'                      => 'Email inviata ai nostri uffici',
    'announcementPubblish'              => 'Announcement Pubblish',
    'announcementPubblishError'         => 'Announcement Pubblish Error',
];
