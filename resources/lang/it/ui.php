<?php
// {{ __('ui.') }}
return [
    // Navbar
    'welcome'   =>'Benvenuti su Presto.it!',
    'createAnnouncement' => 'Crea un annuncio',

    'login' => 'Accedi',
    'register' => 'Registrati',
    'management' => 'Gestione',
    'admin' => 'Amministratore',
    'Logout' => 'Logout',
    'contact' => 'Contattaci',

    // User Home
    'search'=>'Cerca',
    // 'selectedCategory'=>'Categoria selezionata',


    // Revisor Home
    'noAnnouncement'=>'Non ci sono annunci da revisonare',
    'discardAnnouncement'=>' Non ci sono annunci scartati ',
    'details' => 'Dettagli',
    'restore' => 'Ripristina',
    'delete'=>'Elimina',
    'accept'=>'Accetta',
    'reject'=>'Rifiuta',

    //Card
    'addToCart'=>'Aggiungi al carrello',

    //Verify
    'Verify Your Email Address' => 'Verifica la tua mail',

    //announcement create
    'createAnnouncement'        => 'crea un nuovo annuncio',
    'allAnnouncements'          => 'tutti gli annunci',

    'title'                     => 'titolo',
    'price'                     => 'prezzo',
    'body'                      => 'contenuto',
    'images'                    => 'immagini',
    'categories'                => 'categorie',
    'submit'                    => 'invia',

    //contacts
    'contacts'          =>'vuoi diventare un amministratore? Contattaci',
    'emailContent'      =>'Scrivici',

    //login
    'emailAddress'      => 'indirizzo e-mail',
    'forgotPassword'    =>  'password dimenticata',
    'rememberMe'        =>  'Ricordami',

    //register
    'name'                 =>'nome',
    'confirmPassword'      =>'conferma password',

    //verivy
    'emailVerify'       => 'Verifica il tuo indirizzo email',
    'emailBody'         => 'Controlla la tua posta elettronica e clicca su verify email. Se non hai ricevuto niente, ',
    'emailRequest'      => 'Clicca qui per ricevere un altra e-mail',


    //flash messages
    'checkVerify'                       => 'Devi verificare il tuo indirizzo email per poter aver accesso a tutte le nostre risorse',
    'destroy'                           => 'Annuncio eliminato con successo',
    'destroyError'                      => 'Errore nella rimozione dell\'annunncio',
    'notFound'                          => 'Annuncio non trovato',
    'successEmail'                      => 'Email inviata ai nostri uffici',
    'announcementPubblish'              =>  'Annuncio pubblicato',
    'announcementPubblishError'         =>  'Errore nella pubblicazione dell\'annuncio',
];
