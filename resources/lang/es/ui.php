<?php

return [
    'welcome'   =>'Benvienido Presto.it!',
    'createAnnouncement' => 'Crear un anuncio',
    'login' => 'Acceso',
    'register' => 'Registrarse',
    'management' => 'Administraciòn',
    'admin' => 'Administrador',
    'Logout' => 'Logout',
    'contact' => 'Contactènos',
    'search'=>'Buscar',
    // 'selectedcategory'=>'Categorìa seleccionada',
    'noAnnouncement'=>'No hay anuncios para revisar',
    'discardAnnouncement'=>' No hay anuncios descardados ',
    'details' => 'Detalles',
    'restore' => 'Restaurar',
    'delete'=>'Eliminar',
    'accept'=>'Acceptar',
    'reject'=>'Rechaza',
    'addToCart'=>'Anadir al carrito',


    //create Announcement
    'createAnnouncement'        => 'crear un anuncio',
    'allAnnouncements'          => 'todos los anuncios',

    'title'                     => 'tìtulo',
    'price'                     => 'precio',
    'body'                      => 'cuerpo',
    'images'                    => 'imagenas',
    'categories'                => 'categorias',
    'submit'                    => 'entregar',


    //Contacts
    'contacts'          =>'quieres convertirte en administrador? Contáctanos',
    'emailContent'      =>'y escríbenos',


    //login
    'emailAddress'      =>  'dirección de correo electrónico',
    'forgotPassword'    =>  'Has olvidado tu contraseña',
    'rememberMe'        =>  'Requerdame',


    //register
    'name'                 =>'nombre',
    'confirmPassword'      =>'Confirmar contraseña',


    //verivy
    'emailVerify'       => 'Verifique su dirección de correo electrónico',
    'emailBody'         => 'Antes de continuar, verifique su correo electrónico para ver si hay un enlace de verificación. Si no recibió el correo electrónico,',
    'emailRequest'      => 'haga clic aquí para recibir otro',


    //flash messages
    'checkVerify'                       => 'debe verificar su correo electrónico antes de acceder',
    'destroy'                           => 'Anucio destruido',
    'destroyError'                      => 'Error en el anuncio de destroyd',
    'notFound'                          => 'Anuncio no encontrado',
    'successEmail'                      => 'Correo electrónico enviado a nuestras oficinas',
    'announcementPubblish'              => 'Anuncio publicado',
    'announcementPubblishError'         => 'Anuncio publicado error',

];
