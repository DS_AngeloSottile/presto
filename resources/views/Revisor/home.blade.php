<x-layout>

        <div class="my-separator"> </div>
        <x-messages.message> </x-messages>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <button class="nav-link active" id="nav-home-tab" data-bs-toggle="tab" data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home" aria-selected="true">Annunci da revisionare</button>
                        <button class="nav-link" id="nav-profile-tab" data-bs-toggle="tab" data-bs-target="#nav-profile" type="button" role="tab" aria-controls="nav-profile" aria-selected="false">Annunci scartati</button>
                    </div>
                </nav>

                <div class="tab-content" id="nav-tabContent">

                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">

                        @if (!$announcementToRevisioned)
                            <div class="revisor">
                                {{ __('ui.noAnnouncement') }}
                            </div>
                        @else
                        <div class="container-fluid">
                            <div class="row d-flex justify-content-center">

                                <div class="col-10 mb-5"><!--Carta -->
                                    <div class="my-card p-3">
                                        <div class="row justify-content-between"><!--intestazione -->
                                            <div class="col-5"><!--User -->
                                                <div>
                                                    <h3 class=""><i class="fas fa-user-circle"></i> {{ $announcementToRevisioned->user->name }}</h3>
                                                </div>
                                            </div>
                                            <div class="col-5 text-end p-2 me-3"><!--data-->
                                                <h3 class="fs-6">{{ $announcementToRevisioned->created_at->format('Y-m-d') }}</h3>
                                            </div>
                                        </div>

                                        <div class="row">

                                            @foreach ($announcementToRevisioned->images as $image)
                                                <div class="col-12 col-md-6">

                                                    <img src="{{ $image->getUrl(300,150) }}" class="card-img-top" alt="">

                                                </div>

                                                <div class="col-12 col-md-6">

                                                    Adult: {{ $image->adult }}
                                                    spoof: {{ $image->spoof }}
                                                    medical: {{ $image->medical }}
                                                    violence: {{ $image->violence }}
                                                    racy: {{ $image->racy }}

                                                    <b>LABEL</b>
                                                    <ul>
                                                        @if($image->labels)
                                                            @foreach ($image->labels as $label)
                                                                <span class="fw-bolder me-1">#{{ $label }}</span>
                                                            @endforeach
                                                        @endif
                                                    </ul>

                                                </div>
                                                <hr>

                                            @endforeach


                                        </div>

                                        <div class="col-12">

                                            <div class="row">
                                                <div class="col-12 col-md-8"><!--Titolo -->
                                                    <div>
                                                        <h2 class="card-title mt-2 p-2 fw-bold">{{ $announcementToRevisioned->title }}</h2>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-4 text-center align-items-center p-2 mr-2"><!--categoria -->
                                                    <a href="{{ route('searchByCategory',$announcementToRevisioned->category->id) }}" class="categoryLink">
                                                        {{ $announcementToRevisioned->category->name }}
                                                    </a>
                                                </div>
                                                <div class="col-12"><!--descrizione accordion -->
                                                    <div class="accordion accordion-flush" id="accordionFlushExample">
                                                        <div class="accordion-item  bg-transparent"><!--background descrizione -->
                                                        <h2 class="accordion-header" id="flush-headingOne">
                                                            <button class="accordion-button collapsed bg-transparent" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                                                                {{ __('ui.details') }}
                                                            </button>
                                                        </h2>
                                                        <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                                                            <div class="accordion-body"><a href="{{ route('user.announcement.show',$announcementToRevisioned) }}" class="goTo"> <p class="card-text cardBody fs-4">{{ $announcementToRevisioned->body }}</p> </a></div>
                                                        </div>
                                                        </div>


                                                    </div>
                                                </div>

                                                <div class="col-4"><!--prezz0 -->
                                                    <h3 class="info">{{ $announcementToRevisioned->price }}</h3>

                                                </div>

                                                <div class="d-flex justify-content-around">
                                                    <form action=" {{ route('revisor.acceptAnnouncement',$announcementToRevisioned) }}" method="post">
                                                        @csrf
                                                        <button type="submit" class="btn btn-info">{{ __('ui.accept') }}</button>
                                                    </form>

                                                    <form action=" {{ route('revisor.rejecttAnnouncement',$announcementToRevisioned) }}" method="post">
                                                        @csrf
                                                        <button type="submit" class="btn btn-warning">{{ __('ui.reject') }}</button>
                                                    </form>
                                                </div>

                                            </div>
                                        </div>


                                    </div>
                                </div>

                            </div>
                        </div>
                        @endif

                    </div>


                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <div class="container-fluid">
                            <div class="row">
                                @if (!$announcementsTrashed->count() > 0)
                                    <div class="revisor">
                                        Non ci sono annunci scartati
                                    </div>
                                @else
                                    @foreach ($announcementsTrashed as $announcementTrashed)

                                        <div class="col-12 col-md-6 mb-5"><!--Carta -->
                                            <div class="my-card p-3">
                                                <div class="row justify-content-between"><!--intestazione -->
                                                    <div class="col-5"><!--User -->
                                                        <div>
                                                            <h3 class=""><i class="fas fa-user-circle"></i> {{ $announcementTrashed->user->name }}</h3>
                                                        </div>
                                                    </div>
                                                    <div class="col-5 text-end p-2 me-3"><!--data-->
                                                        <h3 class="fs-6">{{ $announcementTrashed->created_at->format('Y-m-d') }}</h3>
                                                    </div>
                                                </div>

                                                <div class="row">

                                                    @foreach ($announcementTrashed->images as $image)
                                                        <div class="col-12 col-md-6">

                                                            <img src="{{ $image->getUrl(300,150) }}" class="card-img-top" alt="">

                                                        </div>

                                                        <div class="col-12 col-md-6">

                                                            Adult: {{ $image->adult }}
                                                            spoof: {{ $image->spoof }}
                                                            medical: {{ $image->medical }}
                                                            violence: {{ $image->violence }}
                                                            racy: {{ $image->racy }}

                                                        </div>
                                                        <div class="col-12">

                                                            <b>LABEL</b>
                                                            <ul>
                                                                @if($image->labels)
                                                                    @foreach ($image->labels as $label)
                                                                        <span class="fw-bolder me-1">#{{ $label }}</span>
                                                                    @endforeach
                                                                @endif
                                                            </ul>
                                                        </div>
                                                        <hr>


                                                    @endforeach


                                                </div>

                                                <div class="col-12">

                                                    <div class="row">
                                                        <div class="col-12 col-md-8"><!--Titolo -->
                                                            <div>
                                                                <h2 class="card-title mt-2 p-2 fw-bold">{{ $announcementTrashed->title }}</h2>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-md-4 text-center align-items-center p-2 mr-2"><!--categoria -->
                                                            <a href="{{ route('searchByCategory',$announcementTrashed->category->id) }}" class="categoryLink">
                                                                {{ $announcementTrashed->category->name }}
                                                            </a>
                                                        </div>
                                                        <div class="col-12"><!--descrizione accordion -->
                                                            <div class="accordion accordion-flush" id="accordionFlushExample">
                                                                <div class="accordion-item  bg-transparent"><!--background descrizione -->
                                                                <h2 class="accordion-header" id="flush-headingOne">
                                                                    <button class="accordion-button collapsed bg-transparent" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne-{{$announcementTrashed->slug}}" aria-expanded="false" aria-controls="flush-collapseOne">
                                                                        {{ __('ui.details') }}
                                                                    </button>
                                                                </h2>
                                                                <div id="flush-collapseOne-{{$announcementTrashed->slug}}" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                                                                    <div class="accordion-body"><a href="{{ route('user.announcement.show',$announcementTrashed) }}" class="goTo"> <p class="card-text cardBody fs-4">{{ $announcementTrashed->body }}</p> </a></div>
                                                                </div>
                                                                </div>


                                                            </div>
                                                        </div>

                                                        <div class="col-4"><!--prezz0 -->
                                                            <h3 class="info">{{ $announcementTrashed->price }}</h3>

                                                        </div>

                                                        <div class="d-flex justify-content-around">
                                                            <form action=" {{ route('revisor.restoreAnnouncement',$announcementTrashed) }}" method="post">
                                                                @csrf
                                                                <button type="submit" class="btn btn-success">{{ __('ui.restore') }}</button>
                                                            </form>

                                                            <form action=" {{ route('revisor.deleteAnnouncement',$announcementTrashed) }}" method="post">
                                                                @csrf
                                                                <button type="submit" class="btn btn-danger">{{ __('ui.delete') }}</button>
                                                            </form>
                                                        </div>

                                                    </div>
                                                </div>


                                            </div>
                                        </div>

                                    @endforeach
                                    {{ $announcementsTrashed->links() }}
                                @endif

                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>

</x-layout>
