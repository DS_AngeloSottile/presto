<x-layout>

        <header>
            <div class="container-fluid">
                <div class="row  align-items-center mybackground header justify-content-center">

                        <x-messages.message>
                        </x-messages>


                    <div class="col-12 col-md-6  text-center text-md-end mt-auto mt-md-0 ">
                        <h3 class="mylogo">
                            Presto
                            <!--   <i class="fas fa-search glass"></i> -->
                        </h3>
                    </div>

                    <div class="col-12 col-md-6 mb-auto mb-md-0   mediaSearch align-items-center">
                        <form action="{{ route('search') }}" method="get" class=" d-inline">
                            <div class="form-group p-2">
                                <div class="   mediaSearch">

                                    <input type="text" name="q" class="form-control m-0 p-2" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="{{ __('ui.search') }}">
                                    <button class="btn btn-primary ms-1" type="submit">{{ __('ui.search') }}</button>

                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </header>

        <div class="container-fluid">
            <div class="row  d-flex justify-content-center">
                @if( isset($categoryToSearch) ) {{-- una variabile di php, serve a controllare se esiste una variabile, se esiste fa la condizione se vera --}}
                categoria selezionata: {{ $categoryToSearch->name }}
                @endif
                @foreach ($categories as $categorie)
                    <div class="col-6 col-sm-4 col-md-2">
                            <a class="btn mt-2 p-0 d-flex align-items-center" href="{{ route('searchByCategory',$categorie->id) }}" role="button">
                                <div class="mybutton text-center  d-flex align-items-center fw-bolder fs-5"><span class="">{{$categorie->name}}</span>
                                </div>
                            </a>
                    </div>
                @endforeach
            </div>
        </div>



        <div class="container-fluid">
            <div class="row mt-5">
                @foreach ($announcements as $announcement)
                <div class="col-12 col-md-4 mb-5">
                    <!--Carta -->
                    <x-card :announcement="$announcement" />{{-- body="{{ $announcement->body }}" --}}
                </div>
                @endforeach
            </div>
        </div>
</x-layout>
