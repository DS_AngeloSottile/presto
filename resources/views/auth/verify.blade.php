<x-layout>

    <div class="my-separator"></div>
<div class="container">
    <div class="row justify-content-center myrow">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('ui.emailVerify') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif

                    {{ __('ui.emailBody') }}
                    <form class="d-inline" method="POST" action="{{ route('verification.send') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('ui.emailRequest') }}</button>.
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

</x-layout>
