<x-layout>

    <div class="my-separator"> </div>
    <div class="container col-md-10 col-md-offset-2">
        <div class="card mt-5">
            <div class="card-header ">
                <h5 class="float-left">{{ __('ui.createAnnouncement') }}</h5>
                <a href="{{ route('user.announcement.index') }}" class="btn btn-warning">{{ __('ui.allAnnouncements') }}</a>
            </div>
            <div class="card-body mt-2">
                <form method="post" action="{{ route('user.announcement.store') }}">
                    @csrf

                    <input type="hidden" name="uniqueSecret" value="{{ $uniqueSecret }}">

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="form-group">
                        <label for="title" class="col-lg-12 control-label">{{ __('ui.title') }}</label>
                        <div class="col-lg-12">
                            <input type="text" class="form-control" id="title" placeholder="{{ __('ui.title') }}" name="title" value="{{old('title') }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="body" class="col-lg-12 control-label">{{ __('ui.body') }}</label>
                        <textarea class="form-control" placeholder="{{ __('ui.body') }}" name="body">{{ old('body') }}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="price" class="col-lg-12 control-label">{{ __('ui.price') }}</label>
                        <div class="col-lg-12">
                            <input type="number" class="form-control" id="price" placeholder="{{ __('ui.title') }}" name="price" value="{{old('price') }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="categories" class="col-lg-12 control-label">{{ __('ui.categories') }}</label>
                        <div class="col-lg-12">
                            <select class="form-control" id="category" name="categories">
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">
                                        {{ $category->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">

                        <label for="images" class="col-md-12 col-form-label text-md-right">{{ __('ui.images') }}</label>

                        <div class="col-md-12">
                            <div class="dropzone" id="drophere"></div> <!-- creo il componente con questo id-->

                            @error('images')
                                <span class="invalid-feedback" role="alert">
                                    <strong> {{ $message }} </strong>
                                </span>
                            @enderror
                        </div>

                    </div>

                    <div class="form-group">
                        <div class="col-lg-10 col-lg-offset-2">
                            <button type="submit" class="btn btn-primary">{{ __('ui.submit') }}</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

</x-layout>
