<x-layout>

    <div class="my-separator"> </div>
    <section class="mainShow d-flex flex-column justify-content-around">

        <div class="my-card p-3">
            <div class="row justify-content-between"><!--intestazione -->
                <div class="col-5"><!--User -->
                    <div>
                        <h3 class=""><i class="fas fa-user-circle"></i> {{$announcement->user->name}}</h3>
                    </div>
                </div>
                <div class="col-5 text-end p-2 me-3"><!--data-->
                    <h3 class="fs-6">{{$announcement->created_at->format('Y-m-d')}}</h3>
                </div>
            </div>
                <div id="secondImage">
                    <img src="{{ $announcement->images[0]->getUrl(300,150) }}" class="" alt="">
                </div>

            <div class=""><!--scheda prodotto -->
                <div class="row">
                    <div class="col-12 col-md-8"><!--Titolo -->
                        <div>
                            <h2 class="card-title mt-2 p-2 fw-bold">{{$announcement->title}}</h2>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 text-center align-items-center p-2 mr-2"><!--categoria -->
                        <a href="{{ route('searchByCategory',$announcement->category) }}" class="categoryLink">
                            {{$announcement->name}}
                        </a>
                    </div>
                    <div class="col-12"><!--descrizione accordion -->
                        <div class="accordion accordion-flush" id="accordionFlushExample">
                            <div class="accordion-item  bg-transparent"><!--background descrizione -->
                              <h2 class="accordion-header" id="flush-headingOne">
                                <button class="accordion-button collapsed bg-transparent" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                                  Dettagli
                                </button>
                              </h2>
                              <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body"><a href="{{ route('user.announcement.show',$announcement) }}" class="goTo"> <p class="card-text cardBody fs-4">{{$announcement->body}}</p> </a></div>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-4"><!--prezz0 -->
                        <h3 class="info">{{$announcement->price}}</h3>

                    </div>
                    <div class="col-8 text-end"><!--bottone carrello -->
                        <button type="button" class="btn btn-warning">Aggiungi al carrello</button>
                    </div>

                </div>

            </div>
        </div>


        <div class="container-fluid caroulseShow">

            <div class="row justify-content-center h-100">
                <div class="col-6 h-100">
                    <div id="carouselExampleIndicators" class="carousel slide h-100" data-bs-ride="carousel">
                        <div class="carousel-indicators">
                            @for ($i = 0; $i < $announcement->images->count(); $i++)
                                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="{{ $i }}" class="@if($i == 0) active @endif" aria-current="true" aria-label="Slide {{ $i+1 }}"></button>
                            @endfor
                        </div>
                        <div class="carousel-inner h-100">
                            @foreach ($announcement->images as $i => $image)
                                    <div class="carousel-item @if($i == 0) active @endif">
                                        <img src="{{ $image->getUrl(300,150) }}" class="d-block w-100" alt="..." id="imgCarousel">
                                    </div>
                            @endforeach
                        </div>
                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                        </button>
                    </div>
                </div>
            </div>

        </div>
    </section>




</x-layout>
