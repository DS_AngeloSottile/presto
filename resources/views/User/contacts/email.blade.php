<x-layout>

<div class="my-separator"> </div>
    <section class="contacts">

        <div class="container col-md-10 col-md-offset-2">
            <div class="card mt-5">
                <div class="card-header ">

                    <h5 class="float-left">{{ __('ui.contacts') }}</h5>

                </div>
                <div class="card-body mt-2">
                    <form method="post" action="{{ route('sendEmail') }} ">
                        @csrf

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="form-group">
                            <label for="body" class="col-lg-12 control-label">{{ __('ui.emailContent') }}</label>
                            <textarea class="form-control" placeholder="email" name="email"></textarea>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-10 col-lg-offset-2">
                                <button type="submit" class="btn btn-primary">{{ __('ui.submit') }}</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>

    </section>
</x-layout>
