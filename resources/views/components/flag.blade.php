<form action="{{ route('locale',$slot) }}" method="post">
    @csrf
    <button type="submit" class="nav-link" style="background-color: transparent; border:none">
        <span class="flag-icon flag-icon-{{ $slot }}"></span>
    </button>
</form>
