@if(session( 'success' ) )

    <div class="col-12 myIndex">
        <div class="row">
            <div class="col-xs-12">
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        {{session('success')}}
                </div>
            </div>
        </div>
    </div>
@endif

@if(session( 'warningEmail' ) )
<div class="col-12 myIndex">
        <div class="row">
            <div class="col-xs-12">
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        {{session('warningEmail')}}
                </div>
            </div>
        </div>
    </div>
@endif

@if( session( 'error' ) )
    @php
        $creation_errors = session('error');
    @endphp
    <div class="col-12 myIndex">
        <div class="row">
            <div class="col-xs-12">
                <div class="alert alert-danger alert-dismissible" role="alert">

                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    @isset( $creation_errors['error_message'] )
                        <strong>{{ $creation_errors['error_message'] }}</strong>
                    @endisset

                    @isset( $creation_errors['details'] )
                        <p>
                            {{ $creation_errors['details'] }}
                        </p>
                    @endisset

                </div>
            </div>
        </div>
    </div>
@endif

