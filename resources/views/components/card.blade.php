    <div class="my-card p-3">
        <div class="row justify-content-between"><!--intestazione -->
            <div class="col-5"><!--User -->
                <div>
                    <h3 class=""><i class="fas fa-user-circle"></i> {{$announcement->user->name}}</h3>
                </div>
            </div>
            <div class="col-5 text-end p-2 me-3"><!--data-->
                <h3 class="fs-6">{{$announcement->created_at->format('Y-m-d')}}</h3>
            </div>
        </div>

            <img src="{{ $announcement->images->first()->getUrl(300,150) }}" class="card-img-top" alt="">

        <div class=""><!--scheda prodotto -->
            <div class="row">
                <div class="col-12 col-md-12"><!--Titolo -->
                    <div>
                        <h2 class="card-title mt-2 p-2 fw-bold">{{Str::limit($announcement->title,20)}}</h2>
                    </div>
                    <div class="col-12 col-md-12 align-items-center p-2 mr-2"><!--categoria -->
                        <a href="{{ route('searchByCategory',$announcement->category->id) }}" class="categoryLink">
                            {{ $announcement->category->name }}
                        </a>
                    </div>
                </div>
                <div class="col-12"><!--descrizione accordion -->
                    <div class="accordion accordion-flush" id="accordionFlushExample">
                        <div class="accordion-item  bg-transparent"><!--background descrizione -->
                          <h2 class="accordion-header" id="flush-headingOne">
                            <button class="accordion-button collapsed bg-transparent" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne-{{ $announcement->slug }}" aria-expanded="false" aria-controls="flush-collapseOne">
                                {{ __('ui.details') }}
                            </button>
                          </h2>
                          <div id="flush-collapseOne-{{ $announcement->slug }}" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body"><a href="{{ route('user.announcement.show',$announcement) }}" class="goTo"> <p class="card-text cardBody fs-4">{{Str::limit($announcement->title,15)}}</p> </a></div>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="col-4"><!--prezz0 -->
                    <h3 class="info">{{$announcement->price}}</h3>

                </div>
                <div class="col-8 text-end"><!--bottone carrello -->
                    <button type="button" class="btn btn-warning">{{ __('ui.addToCart') }}</button>
                </div>

            </div>

        </div>
    </div>
