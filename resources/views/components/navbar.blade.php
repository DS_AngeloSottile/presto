<nav class="navbar navbar-expand-md navbar-light navbar-color my-navbar">
    <div class="container-fluid ">
        <a class="navbar-brand" href="{{ url('/') }}">
            <h2 class="header-logo fw-bolder"> {{ __('ui.welcome') }} </h2>
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav me-auto">

                <li class="nav-item ">
                    <a class="nav-link" href="{{ route('user.announcement.create') }}"><h5 class="colorNavbar">{{ __('ui.createAnnouncement') }}</h5></a>
                </li>

                <li class="nav-item dropdown">

                    <a id="navbarDropdown" class="nav-link dropdown-toggle " href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        <span class="colorNavbar">  {{ strtoupper(session('locale','it')) }} </span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right dropDownFlag" aria-labelledby="navbarDropdown">

                        <a class="dropdown-item">
                            <x-flag>{{ "it" }}</x-flag>
                        </a>

                        <a class="dropdown-item">
                            <x-flag>{{ "gb" }}</x-flag>
                        </a>

                        <a class="dropdown-item">
                            <x-flag>{{ "es" }}</x-flag>
                        </a>

                    </div>
                </li>

            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav me-5">
                <!-- Authentication Links -->
                @guest

                @if (Route::has('login'))
                <li class="nav-item ">
                    <a class="nav-link " href="{{ route('login') }}"><h5 class="colorNavbar">{{ __('ui.login') }}</h5></a>
                </li>
                @endif

                @if (Route::has('register'))
                <li class="nav-item">
                    <a class="nav-link " href="{{ route('register') }}"><h5 class="colorNavbar">{{ __('ui.register') }}</h5></a>
                </li>
                @endif

                @else

                <li class="nav-item dropdown">

                    <a id="navbarDropdown" class="nav-link dropdown-toggle user-color" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                     <span class="colorNavbar"> {{ Auth::user()->name }} </span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right mydropDawnChange" aria-labelledby="navbarDropdown">

                        <a class="dropdown-item" href="{{ route('contacts') }}">
                            {{ __('ui.contact') }}
                        </a>

                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>

                    </div>

                </li>


                    @if(Auth::user()->hasRole('revisor'))

                        <li class="nav-item dropdown">

                            <a id="navbarDropdown" class="nav-link dropdown-toggle " href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                              <span class="colorNavbar">  {{ __('ui.management') }}   {{ $announcementsAccepted }} </span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                                <a class="dropdown-item" href="{{ route('revisor.index') }} "> Admin </a>

                            </div>

                        </li>

                    @endif
                @endguest


            </ul>
        </div>
    </div>
</nav>

