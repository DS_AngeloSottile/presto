<?php

use App\Http\Controllers\User\EmailController;
use App\Http\Controllers\User\UserAnnouncementController;
use GuzzleHttp\Middleware;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/search', [App\Http\Controllers\HomeController::class, 'search'])->name('search');
Route::get('/announcementByCategory/{id}', [App\Http\Controllers\HomeController::class, 'searchByCategory'])->name('searchByCategory');


Route::get('/contacts', [App\Http\Controllers\PageController::class, 'contacts'])->name('contacts');
Route::post('/locale/{locale}', [App\Http\Controllers\PageController::class, 'locale'])->name('locale');

Route::post('/sendEmail', [EmailController::class, 'sendEmail'])->name('sendEmail');



Route::post('/register', [App\Http\Controllers\Auth\RegisteredUserController::class, 'store'])->name('register');


Route::prefix('user-announcement')->name('user.')->group( function ()
{
    Route::resource('announcement', App\Http\Controllers\User\UserAnnouncementController::class);
});
Route::post('/announcement/images/upload', [UserAnnouncementController::class, 'uploadImage'])->name('announcement.images.upload');
Route::delete('/announcement/images/remove', [UserAnnouncementController::class, 'deleteImage'])->name('announcement.images.delete');
Route::get('/announcement/images', [UserAnnouncementController::class, 'getImages'])->name('announcement.images');


Route::prefix('revisor-announcement-managment')->name('revisor.')->group( function ()
{
    Route::post('/accept/{announcement}', [App\Http\Controllers\Revisor\RevisorAnnouncementController::class, 'acceptAnnouncement'])->name('acceptAnnouncement');
    Route::post('/reject/{announcement}', [App\Http\Controllers\Revisor\RevisorAnnouncementController::class, 'rejecttAnnouncement'])->name('rejecttAnnouncement');
    Route::post('/restore/{announcement}', [App\Http\Controllers\Revisor\RevisorAnnouncementController::class, 'restoreAnnouncement'])->name('restoreAnnouncement');
    Route::post('/delete/{announcement}', [App\Http\Controllers\Revisor\RevisorAnnouncementController::class, 'deleteAnnouncement'])->name('deleteAnnouncement');

    Route::get('/AnnouncementsRevisor', [App\Http\Controllers\Revisor\RevisorAnnouncementController::class, 'index'])->name('index');
});
